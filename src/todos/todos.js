import { useView } from 'aurelia-framework';

import { Todo } from '../todo/todo';

@useView("./todos.html")
export class Todos {
	heading = 'My first Aurelia TODO App';
	title;

	constructor(){
		this.todos = [];
		this.checkedTodos = [];
	}

	addTodo(){
		let todo = new Todo(this.title);
		this.todos.push(todo);
		this.title = '';
	}

	removeTodo(givenTodo){
		this.todos.forEach((todo,index) =>{
			if(todo.id === givenTodo.id){
				this.todos.splice(index,1);
			}
		});

		this.checkedTodos.forEach((todo,index) =>{
			if(todo.id === givenTodo.id){
				this.checkedTodos.splice(index,1);
			}
		});
	}

	checkTodo(givenTodo){
		if(givenTodo.checked){
			this.checkedTodos.push(givenTodo);
		}else{
			this.checkedTodos.forEach((todo,index) =>{
				if(todo.id === givenTodo.id){
					this.checkedTodos.splice(index,1);
				}
			})
		}
	}
}