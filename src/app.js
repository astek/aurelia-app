export class FirstApp {
	configureRouter(config, router) {
    this.router = router;
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'home'],       name: 'home',       moduleId: 'todos/todos' },
      { route: 'archive',            name: 'archive',      moduleId: 'archive/archive',   nav: true }
    ]);
  }
}