export class Todo {
	description = '';
	checked = false;
	id;

	constructor(title){
		this.description = title;
		this.id = Math.random();
	}
}